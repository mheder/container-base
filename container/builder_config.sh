#!/bin/sh
set -eux

sed -i '0,/^driver.*/s//driver = "vfs"/' /etc/containers/storage.conf
sed -i '0,/# events_logger.*/s//events_logger = "file"/' /usr/share/containers/containers.conf
sed -i '0,/# cgroup_manager.*/s//cgroup_manager = "cgroupfs"/' /usr/share/containers/containers.conf

