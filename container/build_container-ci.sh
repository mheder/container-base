#!/bin/sh
set -eux

build_container ()
{
  ansible-bender build container/ansible/container-ci.yml ${base_image} ${target_image}
}

if [ -z "${CI_COMMIT_TAG+x}" ] ; then
  for os_release in buster
  do
    base_image="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-bare:${CI_COMMIT_REF_NAME}"
    target_image="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-ci:${CI_COMMIT_REF_NAME}"
    build_container
    buildah push "${target_image}"
  done
else
  for os_release in buster
  do
    base_image="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-bare:${CI_COMMIT_REF_NAME}"
    target_image="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-ci"
    major_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1)
    minor_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1,2)
    patch_tag=$(echo "${CI_COMMIT_TAG}" | grep -E '^v[0-9]+\.[0-9]+(\.[0-9]+)?(-.+)?$')
      if [ -z "${patch_tag}" ] ; then
        printf 'Error: the latest '"${CI_COMMIT_TAG}"' tag is not starts with v[0-9].[0-9]%s\n'
        exit 1
      fi
    build_container
    tar -c --directory "${os_release}" . | podman import - "${target_image}:tagged"
    for tag in "${major_tag}" "${minor_tag}" "${patch_tag}"
    do
      buildah tag "${target_image}:${tag}"
      buildah push "${target_image}:${tag}"
    done
  done
fi
